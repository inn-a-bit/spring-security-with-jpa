package kom.get.ready.ws.shared.dto;

import java.io.Serializable;

public class UserDto implements Serializable, Comparable<UserDto>{

	private static final long serialVersionUID = -6012370573511756737L;
	private long id;
	private String password;
	private String userId;
	private String firstName;
	private String lastName;
	private String email;
	private String encryptedPassword;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEncryptedPassword() {
		return encryptedPassword;
	}
	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	@Override
	public boolean equals(Object o)
	{		
		if (o == this) { 
            return true; 
        }
		if (!(o instanceof UserDto)) { 
            return false; 
        } 
		UserDto user = (UserDto) o;
		return this.userId.equals(user.userId);
	}
	
	@Override
	public int hashCode()
	{
		return this.userId.hashCode();
	}
	
	@Override 
	public int compareTo(UserDto user)
	{
		return this.userId.compareTo(user.userId);	
	}
	
	@Override
	public String toString(){
		return this.firstName + "  " + this.lastName;
	}
}
