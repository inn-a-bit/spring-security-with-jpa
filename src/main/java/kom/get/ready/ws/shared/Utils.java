package kom.get.ready.ws.shared;

import java.security.SecureRandom;
import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class Utils {

	private final Random RANDOM = new SecureRandom();
	private final String KEY_BOARD = "qwertyuiopasdfghjklzxcvbnm1234567890";

	public String generateUserId(int length)
	{
		return generateRandomString(length);
	}
	
	private String generateRandomString(int length)
	{
		StringBuilder returnValue = new StringBuilder(length);
		
		for(int i = 0; i < length; i++)
		{
			returnValue.append(KEY_BOARD.charAt(RANDOM.nextInt(KEY_BOARD.length())));
		}
		
		return returnValue.toString();
	}
}
