package kom.get.ready.ws.io.entity;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="authorities")
public class AuthorityEntity {
		
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="authorities_seq")
	@SequenceGenerator(name = "authorities_seq", sequenceName = "auth_sequence", initialValue = 1, allocationSize=1)
	private long id;

	@Column(nullable=false, length=20)
	private String name;
	
	@ManyToMany(mappedBy="authorities")
	private Collection<RoleEntity> roles;

	public AuthorityEntity() {
		// Empty constructor creates a new instance via reflection by the persistence framework.
		// Has to be added, otherwise spring boot will fail on startup. 
	}

	public AuthorityEntity(String name) {
		
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<RoleEntity> getRoles() {
		return roles;
	}

	public void setRoles(Collection<RoleEntity> roles) {
		this.roles = roles;
	}
	
	@Override
	public boolean equals(Object o)
	{		
		if (o == this) { 
            return true; 
        }
		if (!(o instanceof AuthorityEntity)) { 
            return false; 
        } 
		AuthorityEntity user = (AuthorityEntity) o;
		return this.name.equals(user.name);
	}
	
	@Override
	public int hashCode()
	{
		return this.name.hashCode();
	}
	
	@Override
	public String toString(){
		return this.name;
	}
}
