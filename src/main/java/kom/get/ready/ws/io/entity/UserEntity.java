package kom.get.ready.ws.io.entity;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;


@Entity(name="users")
public class UserEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="user_seq")
	@SequenceGenerator(name = "user_seq", sequenceName = "user_sequence", initialValue = 1, allocationSize=1)
	private long id;
	
	@Column(nullable=false)
	private String userId;
	
	@Column(nullable=false, length=50)
	private String firstName;
	
	@Column(nullable=false, length=50)
	private String lastName;
	
	//@Column(nullable=false, length=100, unique=true)
	@Column(nullable=false, length=100)
	private String email;
	
	@Column(nullable=false)
	private String encryptedPassword;
	
	private String emailVerificationToken;
	
	@Column(nullable=false)
	private Boolean emailVerificationStatus = false;
	
	@ManyToMany(cascade = {CascadeType.PERSIST}, fetch = FetchType.EAGER)
	@JoinTable(name="users_roles", 
			joinColumns=@JoinColumn(name="users_id", referencedColumnName="id")
			, inverseJoinColumns=@JoinColumn(name="roles_id", referencedColumnName="id"))
	
	private Collection<RoleEntity> roles;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEncryptedPassword() {
		return encryptedPassword;
	}
	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}
	public String getEmailVerificationToken() {
		return emailVerificationToken;
	}
	public void setEmailVerificationToken(String emailVerificationToken) {
		this.emailVerificationToken = emailVerificationToken;
	}
	public Boolean isEmailVerificationStatus() {
		return emailVerificationStatus;
	}
	public void setEmailVerificationStatus(Boolean emailVerificationStatus) {
		this.emailVerificationStatus = emailVerificationStatus;
	}
	public Boolean getEmailVerificationStatus() {
		return emailVerificationStatus;
	}
	public Collection<RoleEntity> getRoles() {
		return roles;
	}
	public void setRoles(Collection<RoleEntity> roles) {
		this.roles = roles;
	}
	
	@Override
	public boolean equals(Object o)
	{		
		if (o == this) { 
            return true; 
        }
		if (!(o instanceof UserEntity)) { 
            return false; 
        } 
		UserEntity user = (UserEntity) o;
		return this.userId.equals(user.userId);
	}
	
	@Override
	public int hashCode()
	{
		return this.userId.hashCode();
	}
	
	@Override
	public String toString(){
		return this.firstName + "  " + this.lastName;
	}
}
