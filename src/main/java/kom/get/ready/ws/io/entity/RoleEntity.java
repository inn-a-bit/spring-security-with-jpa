package kom.get.ready.ws.io.entity;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ROLES")
public class RoleEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="role_seq")
	@SequenceGenerator(name = "role_seq", sequenceName = "role_sequence", initialValue = 1, allocationSize=1)
	private long id;
	
	@Column(nullable=false, length=20)
	private String name;

	@ManyToMany(mappedBy="roles")
	private Collection<UserEntity> users;
	
	@ManyToMany(cascade = {CascadeType.PERSIST}, fetch = FetchType.EAGER)
	@JoinTable(name="roles_authorities", 
			joinColumns=@JoinColumn(name="roles_id", referencedColumnName="id")
			, inverseJoinColumns=@JoinColumn(name="authorities_id", referencedColumnName="id"))
	
	private Collection<AuthorityEntity> authorities;
	
	public RoleEntity() {

	}

	public RoleEntity(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<UserEntity> getUsers() {
		return users;
	}

	public void setUsers(Collection<UserEntity> users) {
		this.users = users;
	}

	public Collection<AuthorityEntity> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Collection<AuthorityEntity> authorities) {
		this.authorities = authorities;
	}

	@Override
	public boolean equals(Object o)
	{		
		if (o == this) { 
            return true; 
        }
		if (!(o instanceof RoleEntity)) { 
            return false; 
        } 
		RoleEntity user = (RoleEntity) o;
		return this.name.equals(user.name);
	}
	
	@Override
	public int hashCode()
	{
		return this.name.hashCode();
	}
	
	@Override
	public String toString(){
		return this.name;
	}
}
