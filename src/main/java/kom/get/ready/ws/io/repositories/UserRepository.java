package kom.get.ready.ws.io.repositories;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import kom.get.ready.ws.io.entity.UserEntity;

@Repository
public interface UserRepository extends Serializable, CrudRepository<UserEntity, Long> {
	
	Optional<UserEntity> findByEmail(String email); 
	Optional<UserEntity> findByUserId(@Param ("userId") String id); 

}
