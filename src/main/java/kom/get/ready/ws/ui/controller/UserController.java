package kom.get.ready.ws.ui.controller;


import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kom.get.ready.ws.ui.model.response.ErrorMessages;
import kom.get.ready.ws.ui.model.response.OperationStatusModel;
import kom.get.ready.ws.ui.model.response.RequestOperationStatus;
import kom.get.ready.ws.exceptions.UserServiceException;
import kom.get.ready.ws.service.UserService;
import kom.get.ready.ws.shared.dto.UserDto;
import kom.get.ready.ws.ui.model.request.UserDetailsRequest;
import kom.get.ready.ws.ui.model.response.UserResponse;
import kom.get.ready.ws.ui.model.response.UserUpdateResponse;

@RestController
@RequestMapping("users") 
public class UserController {

	@Autowired
	UserService userService;
	
	@GetMapping(path="/{id}")
	public UserResponse getUser(@PathVariable String id)
	{
		UserResponse userResponse = new UserResponse(); 
		UserDto userDto = userService.getUserByUserId(id);
		BeanUtils.copyProperties(userDto, userResponse);
		return userResponse;
	}

	@PostMapping
	public UserResponse createUser(@RequestBody UserDetailsRequest userDetails)
	{	
		UserResponse userResponse = new UserResponse();
		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userDetails, userDto);
		
		UserDto createdUser = userService.createUser(userDto);
		BeanUtils.copyProperties(createdUser, userResponse);
		
		return userResponse;
	}
	
	@Secured("ROLE_ADMIN")
	@PutMapping(path = "/{id}", 
				consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, 
				produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public UserUpdateResponse updateUser(@PathVariable String id, @RequestBody UserDetailsRequest userDetails) {
		

		UserUpdateResponse returnValue = new UserUpdateResponse();

		UserDto userDto = new UserDto();
		userDto = new ModelMapper().map(userDetails, UserDto.class);
		
		UserDto updateUser = userService.updateUser(id, userDto);
		
		returnValue = new ModelMapper().map(updateUser, UserUpdateResponse.class);

		return returnValue;
	}
	
	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel deleteUser(@PathVariable String id) {
		
		OperationStatusModel returnValue = new OperationStatusModel();
		returnValue.setOperationName(RequestOperationName.DELETE.name());
		
		try {
			userService.deleteUser(id);
		}catch(UserServiceException ex)
		{
			returnValue.setOperationResult(ErrorMessages.NO_RECORD_FOUND.getErrorMessage());
			return returnValue;
		}catch(Exception e)
		{
			returnValue.setOperationResult(RequestOperationStatus.ERROR.name());
			throw e;
		}
		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
		return returnValue;
	}
}
