package kom.get.ready.ws.ui.model.response;

public enum RequestOperationStatus {
	ERROR, 
	SUCCESS
}
