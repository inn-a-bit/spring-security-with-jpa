package kom.get.ready.ws;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import kom.get.ready.ws.io.entity.AuthorityEntity;
import kom.get.ready.ws.io.entity.RoleEntity;
import kom.get.ready.ws.io.entity.UserEntity;
import kom.get.ready.ws.io.repositories.AuthorityRepository;
import kom.get.ready.ws.io.repositories.RoleRepository;
import kom.get.ready.ws.io.repositories.UserRepository;
import kom.get.ready.ws.shared.Utils;

@PropertySource("classpath:usersetup.properties")
@Component
public class InitialUserSetup {
	
	@Autowired
	AuthorityRepository authorityRepository;
	
	@Autowired
	RoleRepository rolesRepository;
	
	@Autowired
	Utils utils;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	UserRepository userRepository;
	
	@Value("${principal.surname}")
	private String principalSurname;
	
	@Value("${principal.name}")
	private String principalName;
	
	@Value("${principal.email}")
	private String principalEmail;
	
	@Value("${principal.password}")
	private String principalPassword;
	
	@EventListener
	@Transactional
	public void onApplicationEvent(ApplicationReadyEvent event)
	{
		AuthorityEntity readAuthority = createAuthority("READ_AUTHORITY");
		AuthorityEntity writeAuthority = createAuthority("WRITE_AUTHORITY");
		AuthorityEntity deleteAuthority = createAuthority("DELETE_AUTHORITY");
		RoleEntity userRole = createRole("ROLE_USER", Arrays.asList(readAuthority,
																		writeAuthority ));
		RoleEntity adminRole = createRole("ROLE_ADMIN", Arrays.asList(readAuthority,
																		writeAuthority, 
																		deleteAuthority ));
		
		UserEntity adminUser = new UserEntity();
		adminUser.setEmail(principalEmail);
		adminUser.setFirstName(principalName);
		adminUser.setLastName(principalSurname);
		adminUser.setEncryptedPassword(bCryptPasswordEncoder.encode(principalPassword));
		adminUser.setEmailVerificationStatus(true);
		adminUser.setUserId(utils.generateUserId(15));
		adminUser.setRoles(Arrays.asList(adminRole));
		
		userRepository.save(adminUser);
	}
	
	private AuthorityEntity createAuthority(String name)
	{
		Optional<AuthorityEntity> authority =  authorityRepository.findByName(name);
		
		return authority.orElseGet(() -> { 
											AuthorityEntity ae = new AuthorityEntity(name);
												authorityRepository.save(ae); 
												return ae; 
											});
	}
	
	private RoleEntity createRole(String name, Collection<AuthorityEntity> authorities)
	{
		Optional<RoleEntity> role = rolesRepository.findByName(name);
		return role.orElseGet(() -> {
										RoleEntity re = new RoleEntity(name);
										re.setAuthorities(authorities);
										rolesRepository.save(re);
										return re;
		});
	}
}
