package kom.get.ready.ws.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import kom.get.ready.ws.SpringApplicationContext;
import kom.get.ready.ws.service.UserService;
import kom.get.ready.ws.shared.dto.UserDto;
import kom.get.ready.ws.ui.model.request.UserLoginRequest;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter{
	
	private final AuthenticationManager authenticationManager;
	
	public AuthenticationFilter(AuthenticationManager authenticationManager)
	{
		this.authenticationManager = authenticationManager;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse resp) 
													throws  AuthenticationException
	{
		try {

				UserLoginRequest credentials = new ObjectMapper().readValue(req.getInputStream(), UserLoginRequest.class);
				
				Authentication au =  authenticationManager.authenticate(
											new UsernamePasswordAuthenticationToken(
													credentials.getEmail(),
													credentials.getPassword(),
													new ArrayList<>()));
				return au;
		}catch(Exception e)
		{
			throw new RuntimeException(e);
		}
	}
	@Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication au) throws IOException, ServletException 
	{        
        String userName = ((UserPrincipal) au.getPrincipal()).getUsername();  
        
        String token = Jwts.builder()
                .setSubject(userName)
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.getTokenSecret() )
                .compact();     
        
        UserService userService = (UserService) SpringApplicationContext.getBean("userServiceImpl");
        
        UserDto userDto = userService.getUser(userName);
        
        res.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + token);
        res.addHeader("UserId", userDto.getUserId());
    }  

}
