package kom.get.ready.ws.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import kom.get.ready.ws.io.entity.AuthorityEntity;
import kom.get.ready.ws.io.entity.RoleEntity;
import kom.get.ready.ws.io.entity.UserEntity;

public class UserPrincipal  implements UserDetails{
	
	private static final long serialVersionUID = -1004951088120533130L;
	
	private UserEntity userEntity;
	
	public UserPrincipal(Optional<UserEntity> userEntity)
	{
		this.userEntity = userEntity.get();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		
		List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		List<AuthorityEntity> authorityEntities = new ArrayList<>();
		Collection<RoleEntity> rolesEntityColl = userEntity.getRoles();
		
		if(rolesEntityColl == null) return grantedAuthorities;
		
		rolesEntityColl.forEach( roleEntity -> {
			grantedAuthorities.add(new SimpleGrantedAuthority(roleEntity.getName()));
						authorityEntities.addAll(roleEntity.getAuthorities());
		});

		authorityEntities.forEach( authorityEntity -> 
									grantedAuthorities.add(new SimpleGrantedAuthority(authorityEntity.getName())));
	//	grantedAuthorities.forEach((ga) -> System.out.println("=========== UserPrincipal grantedAuthorities : " + ga.getAuthority()));
		return grantedAuthorities;
	}

	@Override
	public String getPassword() {
		return this.userEntity.getEncryptedPassword();
	}

	@Override
	public String getUsername() {
		return this.userEntity.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
	
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {		
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		
		return true;
	}

	@Override
	public boolean isEnabled() {
		
		return this.userEntity.getEmailVerificationStatus();
	}

}
