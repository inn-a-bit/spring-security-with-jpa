package kom.get.ready.ws.security;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import kom.get.ready.ws.io.repositories.UserRepository;
import kom.get.ready.ws.service.UserService;

@EnableGlobalMethodSecurity(securedEnabled=true)
@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter{
	
	private final UserService userDetailsService; 
	private final UserRepository userRepository;
	
	public WebSecurity(UserService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder, UserRepository userRepository)
	{		
		this.userDetailsService = userDetailsService;
		this.userRepository = userRepository;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http
		// csrf -> Cross Site Request Forgery
		.csrf()
		.disable()
		.authorizeRequests()
		.antMatchers(HttpMethod.POST, SecurityConstants.SIGN_UP_URL)
		.permitAll()
		.antMatchers(HttpMethod.DELETE, "/users/**").hasRole("ADMIN")		
		.anyRequest()
		.authenticated()
		.and()
		.addFilter(getAuthenticationFilter())
		.addFilter(new AuthorizationFilter(authenticationManager(), userRepository))
		// need to add sessionManagement to make http requests stateless, otherwise JWT is cached.
		.sessionManagement()
		.sessionCreationPolicy(SessionCreationPolicy.STATELESS);		
	}
	
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception
	{
		auth.userDetailsService(userDetailsService); 
	}
	
	public AuthenticationFilter getAuthenticationFilter() throws Exception
	{
		
		final AuthenticationFilter filter = new AuthenticationFilter(authenticationManager());		
		filter.setFilterProcessesUrl("/users/login");
		return filter;
		
	}
}
