package kom.get.ready.ws.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import kom.get.ready.ws.shared.dto.UserDto;

public interface UserService extends UserDetailsService{
	UserDto createUser(UserDto user);
	UserDto getUser(String email);
	UserDto getUserByUserId(String userId);
	void deleteUser(String userId);
	UserDto updateUser(String userId, UserDto user);
}
