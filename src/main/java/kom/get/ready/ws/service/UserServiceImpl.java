package kom.get.ready.ws.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import kom.get.ready.ws.exceptions.UserServiceException;
import kom.get.ready.ws.io.entity.UserEntity;
import kom.get.ready.ws.io.repositories.UserRepository;
import kom.get.ready.ws.security.UserPrincipal;
import kom.get.ready.ws.shared.Utils;
import kom.get.ready.ws.shared.dto.UserDto;
import kom.get.ready.ws.ui.model.response.ErrorMessages;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	Utils utils;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		
		Optional<UserEntity> userEntity = userRepository.findByEmail(email);
		userEntity.orElseThrow(() -> new UsernameNotFoundException("Not found: " + email));
		
		return new UserPrincipal(userEntity);
	}
	
	@Override
	public UserDto createUser(UserDto user) {
	
		userRepository.findByEmail(user.getEmail()).ifPresent(e -> {
										throw new RuntimeException( "User <" + e.getEmail() + "> already exists");
									});
		
		UserEntity userEntity = new UserEntity();
		BeanUtils.copyProperties(user, userEntity);
		
		String publicUserId = utils.generateUserId(15);
		
		userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(user.getPassword()));		
		userEntity.setUserId(publicUserId);
		
		UserEntity storedUserDetail = userRepository.save(userEntity);
		
		UserDto userReturned = new UserDto();
		BeanUtils.copyProperties(storedUserDetail, userReturned);
		
		return userReturned;
	}

	@Override
	public UserDto getUser(String email)
	{
		Optional<UserEntity> userEntity = userRepository.findByEmail(email);
		userEntity.orElseThrow(() -> {
			// This exception shouldn't be triggered because invalid user will fail during execution of 'attemptAuthentication' method
			// 'getUser' is called from 'successfulAuthentication' method.
										throw new UsernameNotFoundException(email);
									});
		UserDto returnValue = new UserDto();
		BeanUtils.copyProperties(userEntity.get(), returnValue);
		return returnValue;
	}
	
	@Override
	public UserDto getUserByUserId(String userId)
	{
		Optional<UserEntity> userEntity = userRepository.findByUserId(userId);
		userEntity.orElseThrow(() -> {
										throw new UsernameNotFoundException(userId);
									});
		UserDto returnValue = new UserDto();
		BeanUtils.copyProperties(userEntity.get(), returnValue);
		return returnValue;
	}
	
	@Transactional
	@Override
	public void deleteUser(String userId) throws UserServiceException{
		Optional<UserEntity> userEntity = userRepository.findByUserId(userId);

		userEntity.orElseThrow(() ->  new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage()));	

		userRepository.delete(userEntity.get());

	}

	@Override
	public UserDto updateUser(String userId, UserDto user) throws UserServiceException{
		UserDto returnValue = new UserDto();

		Optional<UserEntity> userEntity = userRepository.findByUserId(userId);

		userEntity.orElseThrow(() -> new UserServiceException(ErrorMessages.NO_RECORD_FOUND.getErrorMessage()));			
		
		userEntity.get().setFirstName(user.getFirstName());
		userEntity.get().setLastName(user.getLastName());

		UserEntity updatedUserDetails = userRepository.save(userEntity.get());
		
		returnValue = new ModelMapper().map(updatedUserDetails, UserDto.class);
		return returnValue;
	}

}
