package kom.get.ready.ws.exceptions;

public class UserServiceException extends RuntimeException{

	private static final long serialVersionUID = 1968753030466092053L;

	public UserServiceException(String message)
	{
		super(message);
	}
}
